export default {
    name: 'users',
    fields: [
        {
            name: 'id',
            type: 'increments'
        },
        {
            name: 'username',
            type: 'string'
        },
        {
            name: 'password',
            type: 'string'
        },
        {
            name: 'email',
            type: 'string'
        }
        
    ]
}