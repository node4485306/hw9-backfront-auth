import winston from 'winston';
import { Request, Response, NextFunction } from 'express'
import path from 'path';

 
const logDir = path.join(__dirname, 'logs');

export const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.json()
    ),

    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: path.join(logDir, 'app.log') })
    ]
});


export function logRequests(req: Request, res: Response, next: NextFunction) {
    logger.info(`${req.method} ${req.url}`, { body: req.body });
    next();
}

export function logError(error: any, req: Request, res: Response, next: NextFunction) {
    if (error.message === 'Validation error') {
        logger.warn(error.message);
    } else {
        logger.error(`${error.message}\n${error.stack}`);
    }
    next();
}

