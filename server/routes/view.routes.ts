import { Router } from 'express';
import { PostController } from '../controllers/';

// клас ViewRouter який відповідає за відображення сторінок
class ViewRouter {
    // маршрутизаційний роутер
    public router: Router;
    // екземпляр класу PostController
    private postController: PostController

    constructor() {
        // створення express роутера
        this.router = Router();
        this.postController = new PostController()

        // ініціалізація маршрутів
        this.routes()
    }

    routes() {
        this.router.route('/posts').get(this.postController.getPostsPage)
    }
}

const { router } = new ViewRouter()

export default router
