import { Router } from 'express';
import UsersRouter from './users.routes';
import PostRouter from './post.routes';

interface RoutesInterface {
    [index: string]: Router
  }

const Routes: RoutesInterface = {
  user: UsersRouter,
  post: PostRouter,
}

export default Routes;