import jwt from 'jsonwebtoken';
import DatabaseRepository from "./database.repository";

export const AUTH_SECRET_KEY = 'wfwgbtbvwef';

interface IPayload {
    userId: number
}

export class JwtAuthService {

    private static instance: JwtAuthService;

    constructor() {}

    sign(payload: IPayload) {
        return jwt.sign(payload, AUTH_SECRET_KEY, { expiresIn: '300000' });
    }
    
    async verify(token: string): Promise<IPayload> {
        return new Promise(
            (resolve, reject) => jwt.verify(token, AUTH_SECRET_KEY, async (err, decoded: any) => {
                if (err) {
                    reject(null);
                } else {
                    const user = await DatabaseRepository.read('users', decoded?.userId as number);

                    resolve(user);
                }
            })
        )
    }

      // ініціалізація екземпляра класу(патерн Singleton)
  public static getInstance(): JwtAuthService {
    if (!JwtAuthService.instance) {
        JwtAuthService.instance = new JwtAuthService();
    }
    return JwtAuthService.instance;
  }
}

export default JwtAuthService.getInstance();
