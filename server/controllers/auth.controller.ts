import { Request, Response, NextFunction } from "express";
import { JwtAuthService, DatabaseRepository } from '../services';

export interface IAuthRequest extends Request {
    user?: any;
}

export class AuthController {
    async signIn(req: Request, res: Response) {
        const user = await DatabaseRepository
            .db
            .select()
            .from('users')
            .where('email', req.body.email)
            .returning('*')
            .then((rows: any) => rows[0]);

        if (!user) {
            return res.status(401).send({
                message: 'Unauthorized'
            })
        }
        
        if (user.password !== DatabaseRepository.hash(req.body.password)) {
            return res.status(401).send({
                message: 'Unauthorized'
            })
        }

        const token = JwtAuthService.sign({ userId: user.id });

        return res.json({ token });
    }

    
    async registration(req: Request, res: Response) {
        const user = await DatabaseRepository
            .db
            .select()
            .from('users')
            .where('email', req.body.email)
            .returning('*')
            .then((rows: any) => rows[0]);

        if (user) {
            return res.status(401).send({
                message: 'Користувач з такою електронною адресою вже існує'
            });
        }

        req.body.password = DatabaseRepository.hash(req.body.password);
        const data = await DatabaseRepository.create('users', req.body);
       
        return res.json( req.body.password );
    }

    async getUser(req: Request, res: Response) {
        try {
        // Отримання токену з заголовка або запиту
            const token = req.headers.authorization?.split(' ')[1]; 

            console.log(token);

            // Перевірка токену
            if (!token) {
                return res.status(401).send({ message: 'Unauthorized' });
            }

            // Перевірка токену та отримання даних користувача
            const payload = await JwtAuthService.verify(token);
            
            if (!payload) {
                return res.status(401).send({ message: 'Unauthorized' });
            }
                return res.json(payload);
                
        } catch (error) {
            console.error('Error getting user:', error);
            return res.status(500).send({ message: 'Internal Server Error' });
        }
        
    }

    async authMiddleware(req: IAuthRequest, res: Response, next: NextFunction) {
        const auth = req.headers.authorization;
        const token = auth?.replace('Bearer ', '');

        if (!token) {
            return res.status(401).send({
                message: 'Unauthorized'
            })
        }

        const decoded = await JwtAuthService.verify(token).catch(() => null);

        if (!decoded) {
            return res.status(401).send({
                message: 'Unauthorized'
            })
        }

        req.user = decoded;

        next()
    }
}
