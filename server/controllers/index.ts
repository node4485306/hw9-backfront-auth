import { MainController } from './main.controller';
import { UserController } from './user.controller';
import { PostController } from './post.controller';
import { AuthController } from './auth.controller';

export {
    MainController,
    UserController,
    PostController,
    AuthController
}