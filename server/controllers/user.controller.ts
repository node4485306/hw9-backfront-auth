import { BaseController } from './base.controller';
import { DatabaseService } from '../services';
import { Request, Response } from 'express';
import { AuthController } from './auth.controller';

// клас UserController є нащадком класу BaseController
export class UserController extends BaseController {
  constructor() {
    // виклик конструктора базового класу де вказано назву таблиці з якою буде працювати контролер
    super('users');
  }

  // async registration(req: Request, res: Response) {
  //   try {
  //     // Створення нового запису у базі даних
  //     // req.body - дані запису для створення

  //     // Перевіряємо, чи існує вже користувач з такою електронною адресою
  //     const existingUser = await DatabaseService.readEmail('users', req.body.email );
  //     if (existingUser) {
  //         return res.status(401).json({ message: 'Користувач з такою електронною адресою вже існує' });
  //     }

  //     req.body.password = DatabaseService.hash(req.body.password);
  //     const data = await DatabaseService.create('users', req.body);

  //     // Успішна відповідь з кодом статусу 201 (створено) та об'єктом з повідомленням та створеним записом.
  //     res.status(201);
  //     res.json({ message: `user created`, data });
  //   } catch (error) {
  //     // Обробка помилки при створенні запису у базі даних.
  //     res.status(500);
  //     res.json({ message: `Error creating user` });
  //   }
  // }
}
