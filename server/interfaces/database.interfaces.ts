export interface Field {
    name: string;
    type: string;
  }
  
  export interface Schema {
    name: string;
    fields: Field[];
  }
  