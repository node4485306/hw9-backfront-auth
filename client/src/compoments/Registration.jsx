import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function Registration() {

    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (password !== confirmPassword) {
            console.error('Passwords do not match');
            return;
        }

        try {
            const response = await fetch('http://localhost:8000/api/register', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ username, email, password })
            });

            if (response.ok) {
                console.log('Registration successful');
                window.location.href = '/';
            } else {
                console.error('Registration failed');
            }

        } catch (error) {
            console.error('Error registering:', error);
        }
    };


    return (  

        <div className="container">
            <h2>Реєстрація користувача</h2>
            <form onSubmit={handleSubmit}>
                <div className='input-wrapper'>
                    <label>Ім'я користувача:</label>
                    <input type="text" value={username} onChange={(e) => setUsername(e.target.value)} />
                </div>
                <div className='input-wrapper'>
                    <label>Email:</label>
                    <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                </div>
                <div className='input-wrapper'>
                    <label>Пароль:</label>
                    <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                </div>
                <div className='input-wrapper'>
                    <label>Підтвердити пароль:</label>
                    <input type="password" value={confirmPassword} onChange={(e) => setConfirmPassword(e.target.value)} />
                </div>
                <button type="submit" className='btn'>Зареєструватись</button>
            </form>
            <p>Вже є аккаунт? <Link to="/login">Авторизація</Link></p>
        </div>

    );
}

export default Registration;